const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello CICD-Started from Container builded from Image automation!'))

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))